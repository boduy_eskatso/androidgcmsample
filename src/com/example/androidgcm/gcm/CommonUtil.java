package com.example.androidgcm.gcm;

import android.content.Context;
import android.content.Intent;

public final class CommonUtil { 
  
     public static final String SERVER_URL = "http://localhost/push_notification/public_html/public/gcm"; 
    
     public static final String SENDER_ID = "147513452073"; 
    
     public static final String TAG = "GCM"; 
    
     public static final String DISPLAY_MESSAGE_ACTION = "com.example.androidgcm.DISPLAY_MESSAGE";
    
     public static final String EXTRA_MESSAGE = "message"; 
    
     public static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
