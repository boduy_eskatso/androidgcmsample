package com.example.androidgcm.gcm;

import static com.example.androidgcm.gcm.CommonUtil.SENDER_ID;
import android.content.Context;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;

public class GCM{
  
    public void Init(Object obj){ 
      
        Context cObj = (Context) obj;
        
        GCMRegistrar.checkDevice(cObj); 
        GCMRegistrar.checkManifest(cObj);
        
        final String regId = GCMRegistrar.getRegistrationId(cObj);
        
        if (regId.equals("")) {  
          GCMRegistrar.register(cObj, SENDER_ID);
          Log.d("GCM", "Attempting to Register Device");
        } else { 
          if (GCMRegistrar.isRegisteredOnServer(cObj)) {
              Log.d("GCM", "Already Registered");
          } else {
          
          }
      }
    }
    
    
}
