package com.example.androidgcm; 
  
import com.example.androidgcm.gcm.GCM;

import static com.example.androidgcm.gcm.CommonUtil.DISPLAY_MESSAGE_ACTION;

import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    
    new GCM().Init(this); 
    registerReceiver(mHandleMessageReceiver,
        new IntentFilter(DISPLAY_MESSAGE_ACTION));
   
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }
  
  private final BroadcastReceiver mHandleMessageReceiver =
      new BroadcastReceiver() {
  @Override
  public void onReceive(Context context, Intent intent) {
       Log.d("Device", "BroadCastReceiver onRecieve");
  }
};

}
